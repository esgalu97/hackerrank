import java.util.Scanner;

public class Problem5 {
    /*
    In this challenge, we're going to use loops to help us do some simple math.

    Task:
        Given an integer, N, print its first 10 multiples. Each multiple Nxi
        (where 1<=i<=10) should be printed on a new line in the form:
            N x i = result.
    Input Format:
        A single integer, N.

    Constraints:
        2 <= N <= 20

    Output Format:
        Print 10 lines of output; each line i (where 1<=i<=10) contains the
        result of Nxi in the form:
     */
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int N = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        scanner.close();
        for(int value = 1; value < 11; value++){
            int result = value * N;
            System.out.printf("%d x %d = %d%n" , N, value, result);
        }
    }
}
