import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;


public class Problem14 {
    /**
     * <p>JAVA reflection is a very powerful tool to inspect the attributes of a
     * class in runtime. For example, we can retrieve the list of public fields
     * of a class using getDeclaredMethods().</p>
     *
     * <p>In this problem, you will be given a class Solution in the editor. You
     * have to fill in the incompleted lines so that it prints all the methods of
     * another class called Student in alphabetical order. We will append your
     * code with the Student class before running it.</p>
     *
     * <p>You have to print all the methods of the student class in alphabetical
     *      * order</p>
     * @param args
     *

     */

    public static void main(String[] args){
        Class student = Student.class;
        Method[] methods = student.getDeclaredMethods();

        ArrayList<String> methodNames = new ArrayList();
        for(Method method : methods){
            methodNames.add(method.getName());
        }
        Collections.sort(methodNames);
        for(String name: methodNames){
            System.out.println(name);
        }
    }
}

class Student{
    private String name;
    private String id;
    private String email;

    public String getName() {
        return name;
    }
    public void setId(String id) {
        this.id = id;
    }
    public void setEmail(String email) {
        this.email = email;
    }
}