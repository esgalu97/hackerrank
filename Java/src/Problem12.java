import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class Problem12 {
    /**
     * <p>MD5 (Message-Digest algorithm 5) is a widely-used cryptographic hash
     * function with a 128-bit hash value. Here are some common uses for MD5:</p>
     *
     * <li>To store a one-way hash of a password.</li>
     * <li>To provide some assurance that a transferred file has arrived intact.</li>
     *
     * <p>MD5 is one in a series of message digest algorithms designed by Professor
     * Ronald Rivest of MIT (Rivest, 1994); however, the security of MD5 has been
     * severely compromised, most infamously by the Flame malware in 2012. The
     * CMU Software Engineering Institute essentially considers MD5 to be
     * "cryptographically broken and unsuitable for further use".
     *
     * Given an alphanumeric string, s, denoting a password, compute and print
     * its MD5 encryption value.</p>
     * @param args strings
     * @throws NoSuchAlgorithmException
     */
    public static void main(String[] args) throws NoSuchAlgorithmException {
        /* Read and save the input String */
        Scanner scan = new Scanner(System.in);
        String str = scan.next();
        scan.close();

        /* Encode the String using MD5 */
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(str.getBytes());
        byte[] digest = md.digest();

        /* Print the encoded value in hexadecimal */
        for (byte b : digest) {
            System.out.format("%02x", b);
        }
    }
}
