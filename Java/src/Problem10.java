import java.io.*;
import java.time.LocalDate;

class Result {

    /**
     * Complete the 'findDay' function below. <ul>
     *     <li> The function is expected to return a STRING.
     *     <li> The function accepts following parameters:
     *
     * @param  month  INTEGER month
     * @param  day  INTEGER month
     * @param  year  INTEGER month
     */

    public static String findDay(int month, int day, int year) {
        LocalDate dt = LocalDate.of(year, month, day);
        return ""+dt.getDayOfWeek();
    }

}

public class Problem10 {
    /**
     <p> The Calendar class is an abstract class that provides methods for
     converting between a specific instant in time and a set of calendar fields
     such as YEAR, MONTH, DAY_OF_MONTH, HOUR, and so on, and for manipulating
     the calendar fields, such as getting the date of the next week.

     <p> You are given a date. You just need to write the method, getDay, which
    returns the day on that date. To simplify your task, we have provided a
    portion of the code in the editor.

     <p> For example, if you are given the date August 14th 2017, the method
     should return MONDAY as the day on that date.

     <p> Input Format:  <ul>
        A single line of input containing the space separated month, day and
        year, respectively, in MM DD YYYY format. </ul>
     <p> Output Format:  <ul>
        Output the correct day in capital letters.
     */
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(
                new OutputStreamWriter(System.out));

        String[] firstMultipleInput = bufferedReader.readLine().replaceAll(
                "\\s+$", "").split(" ");

        int month = Integer.parseInt(firstMultipleInput[0]);

        int day = Integer.parseInt(firstMultipleInput[1]);

        int year = Integer.parseInt(firstMultipleInput[2]);

        String res = Result.findDay(month, day, year);

        bufferedWriter.write(res);
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }
}