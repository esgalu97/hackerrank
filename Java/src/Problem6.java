import java.util.Scanner;

public class Problem6 {
    /*
    We use the integers , , and  to create the following series:
    [a + 2^0 * b, a + 2^0 * b + 2^1 * b, ...,
        a + 2^0 * b + 2^1 * b + ... + 2^(n-1) * b]
    You are given q queries in the form of a, b, and n. For each query, print
     the series corresponding to the given a, b, and n values as a single line
      of n space-separated integers.

    Input Format:
        The first line contains an integer, q, denoting the number of queries.
        Each line i of the q subsequent lines contains three space-separated
        integers describing the respective a_i, b_i, and n_i values for that
        query.

     For each query, print the corresponding series on a new line. Each series
      must be printed in order as a single line of n space-separated integers.
     */
    public static void main(String []argh){
        Scanner in = new Scanner(System.in);
        int t=in.nextInt();
        for(int i=0;i<t;i++){
            int a = in.nextInt();
            int b = in.nextInt();
            int n = in.nextInt();

            int value = a;
            for (int contador=0; contador<n; contador++){
                value += Math.pow(2, contador)*b;
                System.out.printf("%s ", value);
            }
            System.out.println();
        }
        in.close();
    }
}
