import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class Problem11 {
    /**
     * <p>Given a double-precision number, <code>payment</code>, denoting an
     * amount of money, use the NumberFormat class' <code>getCurrencyInstance</code>
     * method to convert <code>payment</code> into the US, Indian, Chinese, and French
     * currency formats.</p>
     *
     * <p>Then print the formatted values as follows:<ul>
     *
     * <ul><li> us formattedPayment US.
     * <li> india formattedPayment India.
     * <li> china formattedPayment China.
     * <li> france formattedPayment France.</ul></ul></p>
     *
     * <p>Where <code>formattedPayment</code> is <code>payment</code> formatted according
     * to the appropriate Locale's currency.</p>
     * <p>Note: India does not have a built-in Locale, so you must construct one
     * where the language is en (i.e., English).</p>
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double payment = scanner.nextDouble();
        scanner.close();

        /* Create custom Locale for India.
          I used the "IANA Language Subtag Registry" to find India's country code */
        Locale indiaLocale = new Locale("en", "IN");

        /* Create NumberFormats using Locales */
        NumberFormat us     = NumberFormat.getCurrencyInstance(Locale.US);
        NumberFormat india  = NumberFormat.getCurrencyInstance(indiaLocale);
        NumberFormat china  = NumberFormat.getCurrencyInstance(Locale.CHINA);
        NumberFormat france = NumberFormat.getCurrencyInstance(Locale.FRANCE);


        System.out.println("US: " + us.format(payment));
        System.out.println("India: " + india.format(payment));
        System.out.println("China: " + china.format(payment));
        System.out.println("France: " + france.format(payment));
    }
}
