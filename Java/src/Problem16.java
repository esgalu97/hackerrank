import java.io.*;
import java.util.*;
import java.lang.reflect.*;

import static java.lang.System.in;


public class Problem16 {
    /**
     * <p>You are given a class Solution and its main method in the editor. Your
     * task is to create a class Prime. The class Prime should contain a single
     * method checkPrime.</p>
     *
     * <p>The locked code in the editor will call the checkPrime method with one
     * or more integer arguments. You should write the checkPrime method in such
     * a way that the code prints only the prime numbers.</p>
     *
     * <p>Please read the code given in the editor carefully. Also please do not
     * use method overloading!</p>
     * @param args integer
     * @returns There will be only four lines of output. Each line contains only
     *          prime numbers depending upon the parameters passed to checkPrime
     *          in the main method of the class Solution. In case there is no
     *          prime number, then a blank line should be printed.
     */
    public static void main(String[] args) {
        try{
            BufferedReader br=new BufferedReader(new InputStreamReader(in));
            int n1=Integer.parseInt(br.readLine());
            int n2=Integer.parseInt(br.readLine());
            int n3=Integer.parseInt(br.readLine());
            int n4=Integer.parseInt(br.readLine());
            int n5=Integer.parseInt(br.readLine());
            Prime ob=new Prime();
            ob.checkPrime(n1);
            ob.checkPrime(n1,n2);
            ob.checkPrime(n1,n2,n3);
            ob.checkPrime(n1,n2,n3,n4,n5);
            Method[] methods=Prime.class.getDeclaredMethods();
            Set<String> set=new HashSet<>();
            boolean overload=false;
            for(int i=0;i<methods.length;i++)
            {
                if(set.contains(methods[i].getName()))
                {
                    overload=true;
                    break;
                }
                set.add(methods[i].getName());

            }
            if(overload)
            {
                throw new Exception("Overloading not allowed");
            }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }
}


class Prime {
    void checkPrime(int... numbers) {
        for (int num : numbers) {
            if (isPrime(num)) {
                System.out.print(num + " ");
            }
        }
        System.out.println();
    }

    boolean isPrime(int n) {
        if (n < 2) {
            return false;
        } else if (n == 2) {     // account for even numbers now, so that we can do i+=2 in loop below
            return true;
        } else if (n % 2 == 0) { // account for even numbers now, so that we can do i+=2 in loop below
            return false;
        }
        int sqrt = (int) Math.sqrt(n);
        for (int i = 3; i <= sqrt; i += 2) { // skips even numbers for faster results
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
